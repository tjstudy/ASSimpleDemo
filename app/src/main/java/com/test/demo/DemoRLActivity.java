///**
// * <p>DemoActivity Class</p>
// *
// * @author zhuzhenlei 2014-7-17
// * @version V1.0
// * @modificationHistory
// * @modify by user:
// * @modify by reason:
// */
//package com.test.demo;
//
//import android.app.Activity;
//import android.graphics.PixelFormat;
//import android.os.Bundle;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.Surface;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.view.View;
//import android.widget.FrameLayout;
//
//import com.hikvision.netsdk.ExceptionCallBack;
//import com.hikvision.netsdk.HCNetSDK;
//import com.hikvision.netsdk.NET_DVR_DEVICEINFO_V30;
//import com.test.demo.widget.RLPlaySurfaceView;
//
///**
// * <pre>
// *  ClassName  DemoActivity Class
// * </pre>
// *
// * @author zhuzhenlei
// * @version V1.0
// * @modificationHistory
// */
//public class DemoRLActivity extends Activity implements SurfaceHolder.Callback {
//    private static final String TAG = "DemoRLActivity";
//    private static final String IP = "192.168.1.104";
//    private static final int PORT = 8000;
//    private static final String UNAME = "admin";
//    private static final String UPWD = "gyzn2018";
//
//    private SurfaceView m_osurfaceView = null;
//    private static RLPlaySurfaceView[] playView;
//    private int m_iStartChan;
//    private int m_iChanNum;
//    private int layoutType = 9;//默认显示布局（1，4，9）
//    private int surfaceMax = 9;//最大个数
//    private int m_iLogID = -1;
//    private int m_iPlayID = -1; // return by NET_DVR_RealPlay_V40
//    private int m_iPlaybackID = -1; // return by NET_DVR_PlayBackByTime
//
//    /**
//     * Called when the activity is first created.
//     */
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        CrashUtil crashUtil = CrashUtil.getInstance();
//        crashUtil.init(this);
//        setContentView(R.layout.layout_main);
//        if (!initeSdk()) {
//            this.finish();
//            return;
//        }
//        initView();
//    }
//
//    private void initView() {
//        m_osurfaceView = (SurfaceView) findViewById(R.id.Sur_Player);
//        playView = new RLPlaySurfaceView[surfaceMax];
//        m_osurfaceView.getHolder().addCallback(this);
//        loginDevice();
//    }
//
//    // @Override
//    public void surfaceCreated(SurfaceHolder holder) {
//        m_osurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
//        Log.i(TAG, "surface is created");
//
//        //valid just when single channel preview
//        if (-1 == m_iPlayID && -1 == m_iPlaybackID) {
//            return;
//        }
//        playView[0].getpSurfaceView().m_hHolder = holder;
//        Surface surface = holder.getSurface();
//        if (true == surface.isValid()) {
//            if (m_iPlayID != -1) {
//                if (-1 == HCNetSDK.getInstance().NET_DVR_RealPlaySurfaceChanged(m_iPlayID, 0, holder)) {
//                    Log.e(TAG, "Player setVideoWindow failed!");
//                }
//            } else {
//                if (-1 == HCNetSDK.getInstance().NET_DVR_PlayBackSurfaceChanged(m_iPlaybackID, 0, holder)) {
//                    Log.e(TAG, "Player setVideoWindow failed!");
//                }
//            }
//
//        }
//    }
//
//    // @Override
//    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//    }
//
//    // @Override
//    public void surfaceDestroyed(SurfaceHolder holder) {
//        Log.i(TAG, "Player setVideoWindow release!");
//        if (-1 == m_iPlayID && -1 == m_iPlaybackID) {
//            return;
//        }
//        if (true == holder.getSurface().isValid()) {
//            if (m_iPlayID != -1) {
//                if (-1 == HCNetSDK.getInstance().NET_DVR_RealPlaySurfaceChanged(m_iPlayID, 0, null)) {
//                    Log.e(TAG, "Player setVideoWindow failed!");
//                }
//            } else {
//                if (-1 == HCNetSDK.getInstance().NET_DVR_PlayBackSurfaceChanged(m_iPlaybackID, 0, null)) {
//                    Log.e(TAG, "Player setVideoWindow failed!");
//                }
//            }
//        }
//    }
//
//    private boolean initeSdk() {
//        if (!HCNetSDK.getInstance().NET_DVR_Init()) {
//            Log.e(TAG, "HCNetSDK init is failed!");
//            return false;
//        }
//        HCNetSDK.getInstance().NET_DVR_SetLogToFile(3, "/mnt/sdcard/sdklog/", true);
//        return true;
//    }
//
//    /**
//     * NVR设备登录
//     *
//     * @return login ID
//     * @fn loginDevice
//     * @author zhangqing
//     * @brief login on device
//     */
//    private int loginDevice() {
//        try {
//            if (m_iLogID < 0) {
//                m_iLogID = loginNormalDevice();
//                if (m_iLogID < 0) {
//                    return -1;
//                }
//                ExceptionCallBack oexceptionCbf = getExceptiongCbf();
//                if (oexceptionCbf == null) {
//                    return -1;
//                }
//                if (!HCNetSDK.getInstance().NET_DVR_SetExceptionCallBack(oexceptionCbf)) {
//                    return -1;
//                }
//                return m_iLogID;
//            } else {
//                if (!HCNetSDK.getInstance().NET_DVR_Logout_V30(m_iLogID)) {
//                    Log.e(TAG, " NET_DVR_Logout is failed!");
//                    return -1;
//                }
//                m_iLogID = -1;
//            }
//        } catch (Exception err) {
//            Log.e(TAG, "error: " + err.toString());
//        }
//        return -1;
//    }
//
//    /**
//     * NVR设备登录
//     *
//     * @return login ID
//     * @fn loginNormalDevice
//     * @author zhuzhenlei
//     * @brief login on device
//     */
//    private int loginNormalDevice() {
//        NET_DVR_DEVICEINFO_V30 m_oNetDvrDeviceInfoV30 = new NET_DVR_DEVICEINFO_V30();
//        if (null == m_oNetDvrDeviceInfoV30) {
//            Log.e(TAG, "HKNetDvrDeviceInfoV30 new is failed!");
//            return -1;
//        }
//        int iLogID = HCNetSDK.getInstance().NET_DVR_Login_V30(IP, PORT, UNAME, UPWD, m_oNetDvrDeviceInfoV30);
//        if (iLogID < 0) {
//            Log.e(TAG, "NET_DVR_Login is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
//            return -1;
//        }
//        if (m_oNetDvrDeviceInfoV30.byChanNum > 0) {
//            m_iStartChan = m_oNetDvrDeviceInfoV30.byStartChan;
//            m_iChanNum = m_oNetDvrDeviceInfoV30.byChanNum;
//        } else if (m_oNetDvrDeviceInfoV30.byIPChanNum > 0) {
//            m_iStartChan = m_oNetDvrDeviceInfoV30.byStartDChan;
//            m_iChanNum = m_oNetDvrDeviceInfoV30.byIPChanNum + m_oNetDvrDeviceInfoV30.byHighDChanNum * 256;
//        }
//        Log.i(TAG, "NET_DVR_Login is Successful>>>>>>>>m_iChanNum=" + m_iChanNum + ":m_iStartChan=" + m_iStartChan);
//        ChangeSingleSurFace();
//        return iLogID;
//    }
//
//    /**
//     * surfaceView 创建
//     * 布局1，4，9（监控显示个数）
//     */
//    private void ChangeSingleSurFace() {
//        DisplayMetrics metric = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metric);
//
////        if (playView.length == 0) {////总的，默认显示9布局  wid=widthPixels*1/3
//        for (int i = 0; i < surfaceMax; i++) {
//            if (playView[i] == null) {
//                playView[i] = new RLPlaySurfaceView(this);
//                playView[i].getpSurfaceView().setParam(metric.widthPixels * 2 / 3);//width= wid/2  高度为3wid/4
//                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
//                        FrameLayout.LayoutParams.WRAP_CONTENT,
//                        FrameLayout.LayoutParams.WRAP_CONTENT);
//                params.bottomMargin = playView[i].getpSurfaceView().getM_iHeight() - (i / 2)
//                        * playView[i].getpSurfaceView().getM_iHeight();
//                params.leftMargin = (i % 3) * playView[i].getpSurfaceView().getM_iWidth();
//                params.gravity = Gravity.BOTTOM | Gravity.LEFT;
//                addContentView(playView[i], params);
//                playView[i].setVisibility(View.VISIBLE);
//            }
//        }
//        showPreView();
//    }
//
//    /**
//     * 监控预览
//     */
//    private void showPreView() {
//        DisplayMetrics metric = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metric);
//        //根据当前选择的布局，设置宽高，显示或者不显示布局(计算新布局的宽)
//        for (int i = layoutType; i < surfaceMax; ++i) {
//            playView[i].setVisibility(View.INVISIBLE);
//        }
//        int screenWidth = 0;
//        if (layoutType == 1) {
//            screenWidth = metric.widthPixels * 2;
//        } else if (layoutType == 4) {
//            screenWidth = metric.widthPixels;
//        } else if (layoutType == 9) {
//            screenWidth = metric.widthPixels * 2 / 3;
//        }
//        for (int i = 0; i < layoutType; i++) {//要显示多少个
//            playView[i].getpSurfaceView().setParam(screenWidth);//width= wid/2  高度为3wid/4
//            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
//                    FrameLayout.LayoutParams.WRAP_CONTENT,
//                    FrameLayout.LayoutParams.WRAP_CONTENT);
//            params.bottomMargin = playView[i].getpSurfaceView().getM_iHeight() - (i / 2)
//                    * playView[i].getpSurfaceView().getM_iHeight();
//            params.leftMargin = (i % 2) * playView[i].getpSurfaceView().getM_iWidth();
//            params.gravity = Gravity.BOTTOM | Gravity.LEFT;
//            playView[i].setLayoutParams(params);
//            playView[i].setVisibility(View.VISIBLE);
//            playView[i].getpSurfaceView().startPreview(m_iLogID, m_iStartChan + i);
//        }
//    }
//
//    private ExceptionCallBack getExceptiongCbf() {
//        ExceptionCallBack oExceptionCbf = new ExceptionCallBack() {
//            public void fExceptionCallBack(int iType, int iUserID, int iHandle) {
//                System.out.println("recv exception, type:" + iType);
//            }
//        };
//        return oExceptionCbf;
//    }
//
//    public void on1GridLayout(View view) {
//        layoutType = 1;
//        showPreView();
//    }
//
//    public void on4GridLayout(View view) {
//        layoutType = 4;
//        showPreView();
//    }
//
//    public void on9GridLayout(View view) {
//        layoutType = 9;
//        showPreView();
//    }
//}