package com.test.demo.activity;

import android.content.Intent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import com.test.demo.R;
import com.test.demo.base.BaseActivity;
import com.test.demo.base.Constants;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 全屏预览
 */
public class FullScreenActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.surface_view_full)
    SurfaceView surfaceViewFull;

    @Override
    public int getContentView() {
        return R.layout.activity_full_screen;
    }

    @Override
    protected void init() {
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        int logId = intent.getIntExtra(Constants.EXTRA.EXTRA_LOGID, -1);
        int chan = intent.getIntExtra(Constants.EXTRA.EXTRA_CHAN, -1);
        if (logId == -1 || chan == -1) {
            finish();
            return;
        }
        // TODO: 2019/8/7 监控全屏
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
