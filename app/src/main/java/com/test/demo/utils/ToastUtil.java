package com.test.demo.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import static android.widget.Toast.makeText;

/**
 * @author gkfeng
 * @time 2016/8/30 11:56
 * @desc Toast提示工具
 */
public class ToastUtil {
    private static Toast toast = null;
    private static int TOAST_DURATION = Toast.LENGTH_LONG;

    /**
     * 普通文本消息提示
     *
     * @param context
     * @param text
     * @param duration
     */
    public static void TextToast(Context context, CharSequence text, int duration) {
        //创建一个Toast提示消息
        if (context == null || text == null || TextUtils.isEmpty(text) || "null".equals(text)) {
            return;
        }
        try {
            if (toast == null) {
                toast = makeText(context, text, duration);
            } else {
                toast.setText(text);
                toast.setDuration(duration);
            }
            //设置Toast提示消息在屏幕上的位置
            toast.setGravity(Gravity.CENTER, 0, 0);
            //显示消息
            toast.show();
        } catch (Exception e) {
        }
    }

    /**
     * 普通文本消息提示
     *
     * @param context
     * @param text
     */
    public static void TextToast(Context context, CharSequence text) {
        TextToast(context, text, TOAST_DURATION);
    }

    /**
     * 普通文本消息提示
     *
     * @param context
     * @param text
     */
    public static void TextToastShort(Context context, CharSequence text) {
        Toast toast = Toast.makeText(context, text == null ? "" : text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    /**
     * 带图片消息提示
     *
     * @param context
     * @param ImageResourceId
     * @param text
     */
    public static void ImageToast(Context context, int ImageResourceId, CharSequence text) {
    }

    /**
     * 带图片消息提示
     *
     * @param context
     * @param ImageResourceId
     */
    public static void ImageToast(Context context, int ImageResourceId) {
        ImageToast(context, ImageResourceId, TOAST_DURATION);
    }

    /**
     * 带图片消息提示
     *
     * @param context
     * @param ImageResourceId
     */
    public static void ImageToast(Context context, int ImageResourceId, int duration) {
        //创建一个Toast提示消息
        toast = makeText(context, "", duration);
        //设置Toast提示消息在屏幕上的位置
        toast.setGravity(Gravity.CENTER, 0, 0);
        //创建一个LineLayout容器
        //创建一个ImageView
        ImageView img = new ImageView(context);
        img.setImageResource(ImageResourceId);
        //将ImageView容器设置为toast的View
        toast.setView(img);
        //显示消息
        toast.show();
    }
}
