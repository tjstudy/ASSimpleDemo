package com.test.demo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.test.demo.PlaySurfaceView;
import com.test.demo.R;
import com.test.demo.utils.ScreenUtils;

/**
 * 带边框的surfaceview
 * Created by tjs on 2019/8/5.
 */

public class RLPlaySurfaceView extends RelativeLayout {

    private View vSurBg;
    private PlaySurfaceView pSurfaceView;
    private boolean mIsSelected;
    private TextView tvTitle;
    private Context mContext;

    public RLPlaySurfaceView(Context context) {
        this(context, null);
    }

    public RLPlaySurfaceView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public RLPlaySurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        //加载视图的布局
        View view = inflate(context, R.layout.item_rl_playsurfaceview, this);
        vSurBg = view.findViewById(R.id.rl_bg);
        pSurfaceView = (PlaySurfaceView) view.findViewById(R.id.p_surface_view);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public int getTitleHeight() {
        return ScreenUtils.dip2px(mContext, 36);
    }

    /**
     * 设置是否选中
     *
     * @param isCheck
     */
    public void setChecked(boolean isCheck) {
        mIsSelected = isCheck;
        vSurBg.setBackgroundResource(mIsSelected ? R.drawable.shape_rect_out_bg_check : R.drawable.shape_rect_out_bg_uncheck);
    }

    /**
     * 是否选中
     *
     * @return
     */
    public boolean isChecked() {
        return mIsSelected;
    }

    public PlaySurfaceView getPlaySurfaceView() {
        return pSurfaceView;
    }
}
