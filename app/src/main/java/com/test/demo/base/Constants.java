package com.test.demo.base;

/**
 * 全根据类
 * Created by tjs on 2019/8/3.
 */

public class Constants {

    public class EXTRA {
        public static final String EXTRA_LOGID = "logId";//nvr登录id
        public static final String EXTRA_CHAN = "chan";//通道
    }
}
